import { Feature, Map } from "ol";
import { FeatureLike } from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import { Cluster, Vector } from "ol/source";
import VectorSource from "ol/source/Vector";
import { Style, Stroke, Fill, Text } from "ol/style";
import CircleStyle from "ol/style/Circle";
import { StyleLike } from "ol/style/Style";
import { useEffect, useMemo, useState } from "react";

interface useClusterParams {
  geoJson: any;
  features: FeatureLike[];
  map: Map;
}

export const useCluster = ({ geoJson, features, map }: useClusterParams) => {
  const [clusterLayer, setClusterLayer] = useState<VectorLayer | undefined>();

  const styleCache: any = {};
  const style = (feature: any) => {
    const size = feature.get("features").length;
    let style = styleCache[size];
    if (!style) {
      style = new Style({
        image: new CircleStyle({
          radius: 10,
          stroke: new Stroke({
            color: "#fff",
          }),
          fill: new Fill({
            color: "#3399CC",
          }),
        }),
        text: new Text({
          text: size.toString(),
          fill: new Fill({
            color: "#fff",
          }),
        }),
      });
      styleCache[size] = style;
    }
    return style;
  };

  useEffect(() => {
    if (map) {
      setClusterLayer(
        addClusterLayer({
          previousLayer: clusterLayer,
          features: features,
          map,
          geoJson,
          style,
        })
      );
    }
  }, [map, features]);

  return { clusterLayer };
};

const addClusterLayer = ({
  previousLayer,
  features,
  style,
  geoJson,
  map,
}: {
  previousLayer: VectorLayer;
  features: FeatureLike[];
  style: StyleLike;
  geoJson: any;
  map: Map;
}): VectorLayer => {
  const newLayer = previousLayer
    ? previousLayer
    : new VectorLayer({
        style: style,
      });

  if (previousLayer != undefined) {
    previousLayer.getSource().clear();
  } else {
    map.addLayer(newLayer);
  }

  (newLayer as any).tag = "features";

  const source = new Vector({
    format: geoJson,
    features: features as Feature<any>[],
  });

  const clusterSource = new Cluster({
    distance: 10,
    source: source,
  });

  newLayer.setSource(clusterSource);

  return newLayer;
};
