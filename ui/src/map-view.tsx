import * as React from "react";
import { useCallback, useEffect, useState } from "react";
import { Feature, Map, MapBrowserEvent, View } from "ol";
import { fromLonLat } from "ol/proj";
import TileLayer from "ol/layer/Tile";
import { OSM, Vector } from "ol/source";

import "ol/ol.css";
import { FeatureLike } from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import { GeoJSON } from "ol/format";
import { Icon, Style } from "ol/style";
import { useCluster } from "./useCluster";
import { boundingExtent } from "ol/extent";

const geoJson = new GeoJSON();

const mapPinStyle = new Style({
  image: new Icon({
    src: "/img/map-pin-blue.png",
    scale: 25 / 50,
    anchor: [0.5, 1.0],
  }),
});

export const MapView: React.FC = () => {
  const [map, setMap] = useState<Map | undefined>(undefined);
  const [featureLayer, setFeatureLayer] = useState<VectorLayer | undefined>();
  const [features, setFeatures] = useState<FeatureLike[]>([]);
  const { clusterLayer } = useCluster({ features, geoJson, map });

  useEffect(() => {
    const map = new Map({
      target: "map",
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      view: new View({
        center: fromLonLat([-0.023758, 51.547504]),
        zoom: 13,
        minZoom: 6,
        maxZoom: 18,
      }),
    });
    map.on("click", onMapClick);

    setMap(map);
    loadFeatureData();
  }, []);

  useEffect(() => {
    if (map) {
      setFeatureLayer(addFeatureLayer(featureLayer, features));
    }
  }, [map, features]);

  const addFeatureLayer = (
    previousLayer: VectorLayer,
    features: FeatureLike[]
  ): VectorLayer => {
    const newLayer = previousLayer
      ? previousLayer
      : new VectorLayer({
          style: mapPinStyle,
        });

    if (previousLayer != undefined) {
      previousLayer.getSource().clear();
    } else {
      map.addLayer(newLayer);
    }

    (newLayer as any).tag = "features";

    const source = new Vector({
      format: geoJson,
      features: features as Feature<any>[],
    });

    newLayer.setSource(source);

    return newLayer;
  };

  const loadFeatureData = () => {
    fetch("/api/geo-json")
      .then((response) => response.json())
      .then((json) => setFeatures(geoJson.readFeatures(json)));
  };

  const onMapClick = (e: MapBrowserEvent) => {
    const coordinate = e.coordinate;

    if (clusterLayer) {
      clusterLayer.getFeatures(e.pixel).then((clickedFeatures) => {
        if (clickedFeatures.length) {
          const features = clickedFeatures[0].get("features");
          if (features.length > 1) {
            const extent = boundingExtent(
              features.map((r: any) => r.getGeometry().getCoordinates())
            );
            map
              .getView()
              .fit(extent, { duration: 1000, padding: [50, 50, 50, 50] });
          }
        }
      });
    }

    fetch("/api/geo-json/add", {
      method: "POST",
      body: JSON.stringify(coordinate),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) =>
        response.ok ? loadFeatureData() : console.log(response)
      )
      .catch((err) => console.log(err));
    e.preventDefault();
    e.stopPropagation();
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div id="map" style={{ height: "90vh", width: "90vw" }} />
    </div>
  );
};
