@file:Suppress("unused")

package com.xerini.interview.server.api

import com.google.gson.Gson
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import java.util.concurrent.CopyOnWriteArrayList
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

@Path("/geo-json")
class GeoJsonService {


    private val gson = Gson()
    private val allCoordinates = CopyOnWriteArrayList<List<Double>>()

    init {
        //If there is no coordinates check the file and add it to allCoordinates
        if (allCoordinates.isEmpty()) {
            try {
                val jsonString = File("./coordinates.json").readText(Charsets.UTF_8)
                //I'm sorry about this TODO use this without casting
                val coordinatesFromFile: List<List<Double>> = gson.fromJson(jsonString, Array::class.java).toList() as List<List<Double>>
                if (coordinatesFromFile.isNotEmpty()) {
                    coordinatesFromFile.map { t -> allCoordinates.add(t) }
                }
            } catch (e: Exception) {
                println(e)
            }
        }
    }

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {
        val features = mutableListOf<GeoJsonFeature>()

        // Could have mapped the array, I think this looks more readable
        for (coordinate in allCoordinates) {
            features.add(GeoJsonFeature(GeometryData(coordinate), emptyMap()))
        }
        return GeoJsonObject(features)
    }

    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(coordinates: List<Double>): Response {
        //REST won't allow for different types so just checking if the list is empty
        if (coordinates.isNotEmpty()) {
            allCoordinates.add(coordinates)
            //After adding the incoming coordinate from UI writing all coordinates to json file
            try {
                PrintWriter(FileWriter("coordinates.json")).use {
                    val jsonString = gson.toJson(allCoordinates)
                    it.write(jsonString)
                }
            } catch (e: Exception) {
                println(e)
            }
            return Response.ok().build()
        }
        return Response.status(400).build()
    }
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}