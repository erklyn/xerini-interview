package com.xerini.interview.server.api

import org.junit.Test
import javax.ws.rs.core.Response
import kotlin.test.assertEquals
import kotlin.test.assertFails

class GeoJsonServiceTest {

    private val service = GeoJsonService()
    @Test
    fun `stores and returns geo-json coordinate data`() {
        val incomingCoordinates = listOf<Double>(12.244, 12.2444)
        service.addPoint(incomingCoordinates)

        val result = service.getGeoJson()

        assertEquals(result,
                GeoJsonObject(
                        listOf(
                                GeoJsonFeature(GeometryData(incomingCoordinates), emptyMap())
                        )
                )
        )
    }
}