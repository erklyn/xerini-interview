
## What did I cover on this task

-  Implemented required tasks (Endpoints and ui onMapClick function)
-  On the backend persisted data on the file system instead of in memory.
-  Added clustering to markers on UI. 

## Tests
-   Only tested backend because currently there is nothing to tested on the UI.
-   Tested adding coordinates , and expecting those coordinates to return.



## What I Would do If I had more free time
- Would have used a SQL, Document based database instead of writing and reading to file system.
- Would have made UI look more like dashboard than just a map
- Would like to add popup when adding a marker so that enduser can name the marker / point